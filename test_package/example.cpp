#include "mkl/mkl.h"
#include "mkl/mkl_cblas.h"

#include <iostream>

int main(int argc, char *argv[])
{
  CBLAS_LAYOUT const layout = CblasRowMajor;
  CBLAS_TRANSPOSE const transA = CblasNoTrans;
  CBLAS_TRANSPOSE const transB = CblasNoTrans;
  MKL_INT const n = 2;
  MKL_INT const m = 2;
  MKL_INT const k = 2;
  MKL_INT const lda = 2;
  MKL_INT const ldb = 2;
  MKL_INT const ldc = 2;
  double const alpha = 1.0;
  double const beta = 0.0;

  double* const a = static_cast<double*>(mkl_calloc(n*n, sizeof(double), 64));
  double* const b = static_cast<double*>(mkl_calloc(n*n, sizeof(double), 64));
  double* const c = static_cast<double*>(mkl_calloc(n*n, sizeof(double), 64));

  a[0] = 1.0; a[1] = 1.0;
  a[2] = 0.0; a[3] = 1.0;

  b[0] = 2.0; b[1] = 3.0;
  b[2] = 0.0; b[3] = 4.0;

  c[0] = 1000.0; c[1] = 1000.0;
  c[2] = 1000.0; c[3] = 1000.0;

  cblas_dgemm(layout, transA, transB, m, n, k, alpha,
              a, lda, b, ldb, beta, c, ldc);

  std::cout << c[0] << " " << c[1] << '\n';
  std::cout << c[2] << " " << c[3] << '\n';

  mkl_free(a);
  mkl_free(b);
  mkl_free(c);

  return 0;
}
