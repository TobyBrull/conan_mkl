from conans import ConanFile, tools
import six, re, os, glob

class IntelMklConan(ConanFile):
    name = "intel_mkl"
    version = "2020.2"
    settings = "os", "arch"
    description = "<Description of IntelMkl here>"
    url = "None"
    license = "None"
    author = "None"
    topics = None

    def obtain_conda_package(self, package_name, version=None):
        print(f"Getting {package_name}")

        used_version = version if (version is not None) else self.version

        buf = six.StringIO()
        msg = self.run(
            f'conda search {package_name}=={used_version} --info | grep "^url"',
            output=buf)
        url = buf.getvalue()
        url = url.split()[2]
        main_url, basename = os.path.dirname(url), os.path.basename(url)
        basename = re.sub(r'.conda$', '', basename)

        if not os.path.exists(f'{basename}.tar.bz2'):
            print(f"Downloading from {url} to {basename}.tar.bz2")
            self.run(f'wget -q {main_url}/{basename}.tar.bz2 -O {basename}.tar.bz2')
        else:
            print('Already downloaded!')

        if not os.path.exists(f'{basename}.tar'):
            print(f"Decompressing {basename}.tar.bz2")
            self.run(f'bunzip2 -k {basename}.tar.bz2')
        else:
            print('Already decompressed!')

        if not os.path.exists(f'{basename}'):
            print(f"Extracting {basename}.tar")
            self.run(f'mkdir {basename}')
            self.run(f'tar -xf {basename}.tar -C {basename}')
        else:
            print('Already extracted!')

        return basename
        self.copy("*.h", src=basename, dst='include', keep_path=False)
        self.copy("*.f90", src=basename, dst='include', keep_path=False)
        self.copy("*.fi", src=basename, dst='include', keep_path=False)

        self.copy("*.so", src=basename, dst='include', keep_path=False)

    def package(self):
        path = self.obtain_conda_package('mkl-include')
        self.copy("*", src=f'{path}/include', dst="include", keep_path=False)

        path = self.obtain_conda_package('mkl')
        self.copy("*", src=f'{path}/lib', dst='lib', keep_path=False)

        path = self.obtain_conda_package('intel-openmp')
        self.copy("*", src=f'{path}/lib', dst='lib', keep_path=False)

        path = self.obtain_conda_package('tbb')
        self.copy("*", src=f'{path}/lib', dst='lib', keep_path=False)

        path = self.obtain_conda_package('tbb-devel')
        self.copy("*", src=f'{path}/include', dst='include', keep_path=True)
        self.copy("*", src=f'{path}/lib', dst='lib', keep_path=True)

    def package_info(self):
        self.cpp_info.libs = [ 'libmkl_rt.so' ]
